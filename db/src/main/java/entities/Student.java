package entities;

public class Student {
    private int StudentId;
    private String name;
    private String phone;
    private int GroupId;

    public int getStudentId() {
        return StudentId;
    }

    public void setStudentId(int studentId) {
        StudentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    @Override
    public String toString() {
        return "entities.Student{" +
                "StudentId=" + StudentId +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", GroupId=" + GroupId +
                '}';
    }
}
