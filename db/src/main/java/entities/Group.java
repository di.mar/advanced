package entities;

public class Group {
    private int GroupId;
    private String GroupName;

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int groupId) {
        GroupId = groupId;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    @Override
    public String toString() {
        return "entities.Group{" +
                "GroupId=" + GroupId +
                ", GroupName='" + GroupName + '\'' +
                '}';
    }
}
